# OpenVX-SC-PLUS

An up-to-date C++ wrapper for the OpenVX C API, that addresses some of the issues that developers may encounter when developing in an Adaptive AUTOSAR or safety-critical environment.