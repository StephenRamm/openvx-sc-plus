/*
 * Copyright 2018 Stephen V C Ramm
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef __OPENVX_DEPLOY_INCLUDED__
#define __OPENVX_DEPLOY_INCLUDED__
#include "vxreference.hpp"
#include "vximport.hpp"
#include "vximage.hpp"
#include "vxlut.hpp"
#include "vxobjectarray.hpp"
#include "vxpyramid.hpp"
#include "vxremap.hpp"
#include "vxarray.hpp"
#include "vxconvolution.hpp"
#include "vxdelay.hpp"
#include "vxgraph.hpp"
#include "vxmatrix.hpp"
#include "vxscalar.hpp"
#include "vxthreshold.hpp"
#include "vxdistribution.hpp"
#include "vxtensor.hpp"

namespace openvx
{
    namespace deployment
    {
        class VxContext : public VxReference
        {
            private:
            VxContext( vx_context ref )
                : VxReference( reinterpret_cast<vx_reference>( ref ), VX_TYPE_CONTEXT )
            {
            }

            friend class VxReference;
            public:
            operator const vx_context() const { return reinterpret_cast<vx_context>( pimpl ); }

            ~VxContext() {}
            // Standard constructor uses OpenVX API to create a context
            VxContext( void )
                : VxReference( reinterpret_cast<vx_reference>( vxCreateContext() ), VX_TYPE_CONTEXT )
            {
            }

            // deployment set Context queries
            template <vx_enum A>
            typename tagmap<A>::vx_type queryContext(typename tagmap<A>::vx_type init={0}) const
            {
                static_assert( (A & VX_TYPE_MASK) == (VX_TYPE_CONTEXT << 8), "You must use an Context type attribute!");
                auto data(init);
                if ( nullptr != pimpl )
                {
                    vxQueryContext( *this, A, &data, sizeof( data ) );
                }
                return data;                
            }

            std::string getImplementation() const
            {
                vx_char name[VX_MAX_IMPLEMENTATION_NAME];
                vxQueryContext( *this, VX_CONTEXT_IMPLEMENTATION, name, sizeof( name ) );
                return name;
            }

            std::string getExtensions() const
            {
                vx_size xsize = 0;
                vxQueryContext( *this, VX_CONTEXT_EXTENSIONS_SIZE, &xsize, sizeof( xsize ) );
                vx_char name[xsize];
                vxQueryContext( *this, VX_CONTEXT_EXTENSIONS, name, xsize );
                return name;
            }

            std::vector<vx_kernel_info_t> getUniqueKernelTable() const
            {
                vx_uint32 count = queryContext<VX_CONTEXT_UNIQUE_KERNELS>();
                std::vector<vx_kernel_info_t> table( count );
                vxQueryContext( *this, VX_CONTEXT_UNIQUE_KERNEL_TABLE, table.data(), count * sizeof( vx_kernel_info_t ) );
                return table;
            }

            vx_status exportObjectsToMemory( VxRefArray& refs, const vx_uint8*& ptr, vx_size& length )
            {
                return vxExportObjectsToMemory( *this, refs.getSize(), refs, refs, &ptr, &length );
            }

            // Factories for objects of all the other classes are defined here

            VxImport importObjectsFromMemory( VxRefArray& refs, const vx_uint8* ptr, vx_size length )
            {
                return VxImport( vxImportObjectsFromMemory( *this, refs.getSize(), refs, refs, ptr, length ) );
            }

            VxImage createImage( vx_uint32 width, vx_uint32 height,
                                 vx_df_image color ) // Standard image creation
            {
                return VxImage( vxCreateImage( *this, width, height, color ) );
            }

            VxImage createUniformImage( vx_uint32 width, vx_uint32 height, const vx_df_image_e color, const vx_pixel_value_t pixel )
            {
                return VxImage( vxCreateUniformImage( *this, width, height, color, &pixel ) );
            }

            VxImage createImageFromHandle( const VxImageHandle& handle )
            {
                return VxImage( vxCreateImageFromHandle( *this, handle.color(), handle, handle, handle.mem_type() ) );
            }

            VxObjectArray createObjectArray( VxReference& exemplar, vx_size count )
            {
                return VxObjectArray( vxCreateObjectArray( *this, exemplar, count ) );
            }

            VxDelay createDelay( VxReference& exemplar, vx_size count )
            {
                return VxDelay( vxCreateDelay( *this, exemplar, count ) );
            }

            // Create a scalar with compile-time Type
            template <vx_enum tag>
            VxScalar<tag> createScalar(typename tagmap<tag>::vx_type val)
            {
                return vxCreateScalar(*this, tag, &val);
            }

            // Create a Scalar when type not known at compile-time
            VxScalar<0> createScalar(vx_enum tag, const void * val)
            {
                return vxCreateScalar(*this, tag, val);
            }

            template <vx_enum T>
            VxLUT<T> createLUT(vx_size count)
            {
                return vxCreateLUT(*this, T, count);
            }

            VxLUT<0> createLUT(vx_enum T, vx_size count)
            {
                return vxCreateLUT(*this, T, count);
            }

            // Create an Array with compile-time type
            template <vx_enum tag>
            VxArray<tag> createArray(vx_size capacity)
            {
                return vxCreateArray(*this, tag, capacity);
            }

            // Create an Array when type not known at compile-time
            VxArray<0> createArray(vx_enum A, vx_size capacity)
            {
                return vxCreateArray(*this, A, capacity);
            }

            // Create a Remap
            VxRemap createRemap(vx_uint32 src_width, vx_uint32 src_height, vx_uint32 dst_width, vx_uint32 dst_height)
            {
                return vxCreateRemap(*this, src_width, src_height, dst_width, dst_height);
            }

            // Create a Pyramid
            VxPyramid createPyramid(vx_size levels, vx_float32 scale, vx_uint32 width, vx_uint32 height, vx_df_image format)
            {
                return vxCreatePyramid(*this, levels, scale, width, height, format);
            }

            // Create a convolution
            VxConvolution createConvolution(vx_size columns, vx_size rows)
            {
                return vxCreateConvolution(*this, columns, rows);
            }
            
            // Create a threshold object
            VxThreshold createThreshold(vx_enum threshType, vx_df_image input_format, vx_df_image output_format)
            {
                return vxCreateThresholdForImage(*this, threshType, input_format, output_format);
            }
            
            // Create a tensor object, the number of dimensions and the type of the data are fixed at compile-time
            template<size_t n_dims, vx_enum data_type>
            VxTensor<data_type> createTensor(std::array<vx_size, n_dims> dims, vx_int8 fixed_point_position)
            {
                return vxCreateTensor(*this, n_dims, dims.data(), data_type, fixed_point_position);
            }
            
            // Create a matrix object. The data_type is compile-time
            template <vx_enum data_type> 
            VxMatrix<data_type> createMatrix(vx_size columns, vx_size rows)
            {
                return vxCreateMatrix(*this, data_type, columns, rows);
            }
            
            // Create a pattern matrix object. The pattern is compile-time
            template <vx_enum pattern >
            VxMatrix<VX_TYPE_UINT8> createMatrixFromPattern(vx_size columns, vx_size rows)
            {
                return vxCreateMatrixFromPattern(*this, pattern, columns, rows);
            }
            
            // Create a matrix object. The pattern is compile-time
            template <vx_enum pattern> 
            VxMatrix<VX_TYPE_UINT8> createMatrixFromPatternAndOrigin(vx_size columns, vx_size rows, vx_size orig_cols, vx_size orig_rows)
            {
                return vxCreateMatrixFromPatternAndOrigin(*this, pattern, columns, rows, orig_cols, orig_rows);
            }
            
            // Create a distribution
            VxDistribution createDistribution(vx_size num_bins, vx_int32 offset, vx_uint32 range)
            {
                return vxCreateDistribution(*this, num_bins, offset, range);
            }
        };
        VxContext VxReference::getContext() { return VxContext( vxGetContext( pimpl ) ); }
    } // namespace deployment
} // namespace openvx
#endif //__OPENVX_DEPLOY_INCLUDED__