/*
 * Copyright 2018 Stephen V C Ramm
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include <VX/vx.h>
#include <VX/vx_import.h>
#include <VX/vx_khr_ix.h>
#include <map>
#include <stdlib.h>
#include <string>
#include <vector>
#include <exception>
#include <type_traits>
#ifndef _INCLUDE_TAGGEDTYPE__
#define _INCLUDE_TAGGEDTYPE__
namespace openvx
{
    namespace deployment
    {
        // forward class declarations

        class VxContext;
        class VxGraph;
        class VxReference;
        
        // Add const to a type T if tag is VX_READ_ONLY
        template <typename T, vx_enum tag> struct const_if_RO 
        { 
            typedef T vx_type; 
            typedef T * vx_ptr;
            typedef T & vx_ref;
        };

        template <typename T> struct const_if_RO<T, VX_READ_ONLY> 
        { 
            typedef const T vx_type; 
            typedef const T * vx_ptr;
            typedef const T & vx_ref;
        };

        // Add const to a type T if tag is VX_READ_ONLY
        template <typename T, vx_enum tag> struct const_if_not_RO 
        { 
            typedef const T vx_type; 
            typedef const T * vx_ptr;
            typedef const T & vx_ref;
        };

        template <typename T> struct const_if_not_RO<T, VX_READ_ONLY> 
        { 
            typedef T vx_type; 
            typedef T * vx_ptr;
            typedef T & vx_ref;
        };

        // a tagMap associates a tag with a type, used for attributes and types generally
        template <vx_enum t> struct tagmap {};
        template <> struct tagmap<VX_TYPE_CHAR>       { typedef vx_char vx_type;};
        template <> struct tagmap<VX_TYPE_INT8>       { typedef vx_int8 vx_type;};
        template <> struct tagmap<VX_TYPE_UINT8>      { typedef vx_uint8 vx_type;};
        template <> struct tagmap<VX_TYPE_INT16>      { typedef vx_int16 vx_type;};
        template <> struct tagmap<VX_TYPE_UINT16>     { typedef vx_uint16 vx_type;};
        template <> struct tagmap<VX_TYPE_INT32>      { typedef vx_int32 vx_type;};
        template <> struct tagmap<VX_TYPE_UINT32>     { typedef vx_uint32 vx_type;};
        template <> struct tagmap<VX_TYPE_INT64>      { typedef vx_int64 vx_type;};
        template <> struct tagmap<VX_TYPE_UINT64>     { typedef vx_uint64 vx_type;};
        template <> struct tagmap<VX_TYPE_FLOAT32>    { typedef vx_float32 vx_type;};
        template <> struct tagmap<VX_TYPE_FLOAT64>    { typedef vx_float64 vx_type;};
        template <> struct tagmap<VX_TYPE_ENUM>       { typedef vx_enum vx_type;};
        template <> struct tagmap<VX_TYPE_SIZE>       { typedef vx_size vx_type;};
        template <> struct tagmap<VX_TYPE_DF_IMAGE>   { typedef vx_df_image vx_type;};
        template <> struct tagmap<VX_TYPE_BOOL>       { typedef vx_bool vx_type;};

        template <> struct tagmap<VX_REFERENCE_TYPE>                { typedef vx_enum vx_type;};
        template <> struct tagmap<VX_REFERENCE_NAME>                { typedef const vx_char* vx_type;};
        template <> struct tagmap<VX_REFERENCE_COUNT>               { typedef vx_uint32 vx_type;};
        template <> struct tagmap<VX_GRAPH_STATE>                   { typedef vx_enum vx_type;};
        template <> struct tagmap<VX_GRAPH_NUMNODES>                { typedef vx_uint32 vx_type;};
        template <> struct tagmap<VX_GRAPH_NUMPARAMETERS>           { typedef vx_uint32 vx_type;};
        template <> struct tagmap<VX_GRAPH_PERFORMANCE>             { typedef vx_perf_t vx_type;};
        template <> struct tagmap<VX_IMAGE_WIDTH>                   { typedef vx_uint32 vx_type;};
        template <> struct tagmap<VX_IMAGE_HEIGHT>                  { typedef vx_uint32 vx_type;};
        template <> struct tagmap<VX_IMAGE_FORMAT>                  { typedef vx_df_image vx_type;};
        template <> struct tagmap<VX_IMAGE_PLANES>                  { typedef vx_size vx_type;};
        template <> struct tagmap<VX_IMAGE_SPACE>                   { typedef vx_enum vx_type;};
        template <> struct tagmap<VX_IMAGE_RANGE>                   { typedef vx_enum vx_type;};
        template <> struct tagmap<VX_IMAGE_MEMORY_TYPE>             { typedef vx_enum vx_type;};
        template <> struct tagmap<VX_IMAGE_IS_UNIFORM>              { typedef vx_bool vx_type;};
        template <> struct tagmap<VX_IMAGE_UNIFORM_VALUE>           { typedef vx_pixel_value_t vx_type;};
        template <> struct tagmap<VX_OBJECT_ARRAY_ITEMTYPE>         { typedef vx_enum vx_type;};
        template <> struct tagmap<VX_OBJECT_ARRAY_NUMITEMS>         { typedef vx_size vx_type;};
        template <> struct tagmap<VX_DELAY_TYPE>                    { typedef vx_enum vx_type;};
        template <> struct tagmap<VX_DELAY_SLOTS>                   { typedef vx_size vx_type;};
        template <> struct tagmap<VX_SCALAR_TYPE>                   { typedef vx_enum vx_type;};
        template <> struct tagmap<VX_LUT_COUNT>                     { typedef vx_size vx_type;};
        template <> struct tagmap<VX_LUT_OFFSET>                    { typedef vx_enum vx_type;};
        template <> struct tagmap<VX_LUT_SIZE>                      { typedef vx_uint32 vx_type;};
        template <> struct tagmap<VX_LUT_TYPE>                      { typedef vx_enum vx_type;};
        template <> struct tagmap<VX_ARRAY_CAPACITY>                { typedef vx_size vx_type;};
        template <> struct tagmap<VX_ARRAY_ITEMSIZE>                { typedef vx_size vx_type;};
        template <> struct tagmap<VX_ARRAY_ITEMTYPE>                { typedef vx_enum vx_type;};
        template <> struct tagmap<VX_ARRAY_NUMITEMS>                { typedef vx_size vx_type;};
        template <> struct tagmap<VX_REMAP_SOURCE_WIDTH>            { typedef vx_uint32 vx_type;};
        template <> struct tagmap<VX_REMAP_SOURCE_HEIGHT>           { typedef vx_uint32 vx_type;};
        template <> struct tagmap<VX_REMAP_DESTINATION_WIDTH>       { typedef vx_uint32 vx_type;};
        template <> struct tagmap<VX_REMAP_DESTINATION_HEIGHT>      { typedef vx_uint32 vx_type;};
        template <> struct tagmap<VX_PYRAMID_LEVELS>                { typedef vx_size vx_type;};
        template <> struct tagmap<VX_PYRAMID_SCALE>                 { typedef vx_float32 vx_type;};
        template <> struct tagmap<VX_PYRAMID_WIDTH>                 { typedef vx_uint32 vx_type;};
        template <> struct tagmap<VX_PYRAMID_HEIGHT>                { typedef vx_uint32 vx_type;};
        template <> struct tagmap<VX_PYRAMID_FORMAT>                { typedef vx_df_image vx_type;};
        template <> struct tagmap<VX_CONVOLUTION_ROWS>              { typedef vx_size vx_type;};
        template <> struct tagmap<VX_CONVOLUTION_COLUMNS>           { typedef vx_size vx_type;};
        template <> struct tagmap<VX_CONVOLUTION_SCALE>             { typedef vx_float32 vx_type;};
        template <> struct tagmap<VX_CONVOLUTION_SIZE>              { typedef vx_size vx_type;};
        template <> struct tagmap<VX_MATRIX_TYPE>                   { typedef vx_enum vx_type;};
        template <> struct tagmap<VX_MATRIX_ROWS>                   { typedef vx_size vx_type;};
        template <> struct tagmap<VX_MATRIX_COLUMNS>                { typedef vx_size vx_type;};
        template <> struct tagmap<VX_MATRIX_SIZE>                   { typedef vx_size vx_type;};
        template <> struct tagmap<VX_MATRIX_ORIGIN>                 { typedef vx_coordinates2d_t vx_type;};
        template <> struct tagmap<VX_MATRIX_PATTERN>                { typedef vx_enum vx_type;};
        template <> struct tagmap<VX_THRESHOLD_TYPE>                { typedef vx_enum vx_type;};
        template <> struct tagmap<VX_THRESHOLD_INPUT_FORMAT>        { typedef vx_enum vx_type;};
        template <> struct tagmap<VX_THRESHOLD_OUTPUT_FORMAT>       { typedef vx_enum vx_type;};
        template <> struct tagmap<VX_DISTRIBUTION_DIMENSIONS>       { typedef vx_size vx_type;};
        template <> struct tagmap<VX_DISTRIBUTION_OFFSET>           { typedef vx_int32 vx_type;};
        template <> struct tagmap<VX_DISTRIBUTION_RANGE>            { typedef vx_uint32 vx_type;};
        template <> struct tagmap<VX_DISTRIBUTION_BINS>             { typedef vx_size vx_type;};
        template <> struct tagmap<VX_DISTRIBUTION_WINDOW>           { typedef vx_uint32 vx_type;};
        template <> struct tagmap<VX_DISTRIBUTION_SIZE>             { typedef vx_size vx_type;};
        template <> struct tagmap<VX_TENSOR_NUMBER_OF_DIMS>         { typedef vx_size vx_type;};
        template <> struct tagmap<VX_TENSOR_DIMS>                   { typedef const vx_size * vx_type;};
        template <> struct tagmap<VX_TENSOR_DATA_TYPE>              { typedef vx_enum vx_type;};
        template <> struct tagmap<VX_TENSOR_FIXED_POINT_POSITION>   { typedef vx_int8 vx_type;};
        template <> struct tagmap<VX_CONTEXT_VENDOR_ID>             { typedef vx_uint16 vx_type;};
        template <> struct tagmap<VX_CONTEXT_VERSION>               { typedef vx_uint16 vx_type;};
        template <> struct tagmap<VX_CONTEXT_UNIQUE_KERNELS>        { typedef vx_uint32 vx_type;};
        template <> struct tagmap<VX_CONTEXT_MODULES>               { typedef vx_uint32 vx_type;};
        template <> struct tagmap<VX_CONTEXT_REFERENCES>            { typedef vx_uint32 vx_type;};
        template <> struct tagmap<VX_CONTEXT_EXTENSIONS_SIZE>                   { typedef vx_size vx_type;};
        template <> struct tagmap<VX_CONTEXT_CONVOLUTION_MAX_DIMENSION>         { typedef vx_size vx_type;};
        template <> struct tagmap<VX_CONTEXT_OPTICAL_FLOW_MAX_WINDOW_DIMENSION> { typedef vx_size vx_type;};
        template <> struct tagmap<VX_CONTEXT_IMMEDIATE_BORDER>                  { typedef vx_border_t vx_type;};
        template <> struct tagmap<VX_CONTEXT_IMMEDIATE_BORDER_POLICY>   { typedef vx_enum vx_type;};
        template <> struct tagmap<VX_CONTEXT_NONLINEAR_MAX_DIMENSION>   { typedef vx_size vx_type;};
        template <> struct tagmap<VX_CONTEXT_MAX_TENSOR_DIMS>           { typedef vx_size vx_type;};
    } // deployment
} // openvx
#endif