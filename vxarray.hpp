/*
 * Copyright 2018 Stephen V C Ramm
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef _INCLUDE_VXARRAY_
#define _INCLUDE_VXARRAY_
#include "taggedtype.hpp"

namespace openvx
{
    namespace deployment
    {
        template <vx_enum tag, vx_enum usage> class VxArrayMap;
        template <vx_enum tag>
        class VxArray : public VxReference
        {
            template <vx_enum, vx_enum> friend class VxArrayMap;
            friend class VxContext;
            VxArray( vx_array ref )
                : VxReference( reinterpret_cast<vx_reference>( ref ), VX_TYPE_ARRAY )
            {
            }
            VxArray( vx_reference ref )
                : VxReference( ref, VX_TYPE_ARRAY )
            {
            }
            public:
            operator const vx_array() const { return reinterpret_cast<vx_array>( pimpl ); }
            ~VxArray() {}

            template <vx_enum A>
            typename tagmap<A>::vx_type queryArray(typename tagmap<A>::vx_type init={0}) const
            {
                auto data(init);
                if ( nullptr != pimpl )
                {
                    vxQueryArray( *this, A, &data, sizeof( data ) );
                }
                return data;                
            }

            template <vx_enum TAG=tag>
            vx_status copyArrayRange(vx_size start, vx_size end, vx_size stride, typename tagmap<TAG>::vx_type &data, vx_enum dir, vx_enum mem_type=VX_MEMORY_TYPE_HOST)
            {
                static_assert(TAG == tag || 0 == tag && 0 != TAG, "Invalid type for VxArray.copyArrayRange");
                auto count = queryArray<VX_ARRAY_NUMITEMS>();
                return (start < end && end <= count) ?
                    vxCopyArrayRange(*this, start, end, stride, &data, dir, mem_type) :
                    VX_ERROR_INVALID_DIMENSION;
            }

            template <vx_enum TAG=tag>
            vx_status copyArrayRange(vx_size start, std::vector<typename tagmap<TAG>::vx_type> &data, vx_enum dir, vx_enum mem_type=VX_MEMORY_TYPE_HOST)
            {
                static_assert(TAG == tag || 0 == tag && 0 != TAG, "Invalid type for VxArray.copyArrayRange");
                return copyArrayRange(*this, start, start + data.size(), sizeof(typename tagmap<TAG>::vx_type), data.data(), dir, mem_type);
            }

            template <vx_enum TAG=tag>
            vx_status copyArrayRange(vx_size start, vx_size count, std::vector<typename tagmap<TAG>::vx_type> &data, vx_enum dir, vx_enum mem_type=VX_MEMORY_TYPE_HOST)
            {
                static_assert(TAG == tag || 0 == tag && 0 != TAG, "Invalid type for VxArray.copyArrayRange");
                return count <= data.size() ?
                        copyArrayRange(*this, start, start + count, sizeof(typename tagmap<TAG>::vx_type), data.data(), dir, mem_type) :
                        VX_ERROR_INVALID_DIMENSION;
            }

            template <vx_enum TAG=tag>
            vx_status copyArray(std::vector<typename tagmap<TAG>::vx_type> &data, vx_enum dir, vx_enum mem_type=VX_MEMORY_TYPE_HOST)
            {
                static_assert(TAG == tag || 0 == tag && 0 != TAG, "Invalid type for VxArray.copyArray");
                return copyArrayRange(*this, 0, data.size(), sizeof(typename tagmap<TAG>::vx_type), data.data(), dir, mem_type);
            }

            template <vx_enum TAG=tag>
            vx_status addArrayItems(std::vector<typename tagmap<TAG>::vx_type> &data)
            {
                static_assert(TAG == tag || 0 == tag && 0 != TAG, "Invalid type for VxArray.addArrayItems");
                return vxAddArrayItems(*this, data.size(), data.data(), sizeof(typename tagmap<TAG>::vx_type));
            }

            template <vx_enum TAG=tag>
            vx_status addArrayItems(vx_size count, std::vector<typename tagmap<TAG>::vx_type> &data)
            {
                static_assert(TAG == tag || 0 == tag && 0 != TAG, "Invalid type for VxArray.addArrayItems");
                return count <=  data.size() ?
                     vxAddArrayItems(*this, count, data.data(), sizeof(typename tagmap<TAG>::vx_type)):
                     VX_ERROR_INVALID_DIMENSION;
            }

            vx_status truncateArray(vx_size new_num_items)
            {
                return vxTruncateArray(*this, new_num_items);
            }

            template <vx_enum usage=VX_READ_AND_WRITE>
            VxArrayMap<tag, usage> mapArray(vx_size start=0, vx_size end=0, vx_enum mem_type_in=VX_MEMORY_TYPE_NONE)
            {
                if (0 == end)
                    end = queryArray<VX_ARRAY_NUMITEMS>();
                return VxArrayMap<tag, usage>(*this, start, end, mem_type_in);
            }    

            template <vx_enum TAG=tag>
            typename tagmap<TAG>::vx_type & operator[](vx_size index)
            {
                return VxArrayMap<tag, VX_READ_AND_WRITE>(*this, index, index+1).template operator[]<TAG>(0); 
            }

        };
        
        template <vx_enum tag, vx_enum usage>
        class VxArrayMap
        {
            friend class VxArray<tag>;
            protected:
            vx_map_id map_id;
            VxArray<tag> array;
            void * ptr=nullptr;
            vx_size count;
            vx_size stride = 0;
            vx_enum status;
            vx_enum item_type = VX_TYPE_INVALID;
            VxArrayMap( VxArray<tag> array_in,
                        vx_size start,
                        vx_size end,
                        vx_enum mem_type_in=VX_MEMORY_TYPE_NONE):
                        array(array_in),
                        count(end - start),
                        status(vxMapArrayRange(array_in, start, end, &map_id, &stride, &ptr, usage, mem_type_in, 0))
            { if (status) throw std::runtime_error("Could not construct VxArrayMap");}
            public:
            ~VxArrayMap()
            {
                if ( VX_SUCCESS == status && array.pimpl != nullptr )
                {
                    status = vxUnmapArrayRange( array, map_id );
                }
            }
            auto getArray() { return array; }
            auto getStatus() { return status; }
            auto getStride() { return stride; }
            auto getCount() { return nullptr == ptr ? 0 : count; }
            auto getType()
            {
                if (0 == tag)
                {
                    if (VX_TYPE_INVALID == item_type)
                        vxQueryArray(array, VX_ARRAY_ITEMTYPE, &item_type, sizeof(item_type));
                    return item_type;
                }
                else
                    return tag;
            }
            template <vx_enum TAG=tag>
            auto getPtr() 
            { 
                static_assert( TAG != 0 && (tag == TAG || tag == 0), "VxArrayMap.getPtr() with wrong or invalid type");
                if ( getType() == TAG)
                    return (typename const_if_RO<typename tagmap<TAG>::vx_type, usage>::vx_ptr)ptr;
                else
                    throw std::invalid_argument("Attempt to get incorrect type from VxArray");
            }
            
            template <vx_enum T=tag>
            auto & operator[](vx_size index)
            {
                if (nullptr == ptr)
                    throw std::invalid_argument("NULL pointer in ArrayMap.ix");
                if (index < getCount() && nullptr != ptr)
                    return getPtr<T>()[index];
                else
                    throw std::invalid_argument("Index out of range for ArrayMap.ix(index)");
            }
        };
    } // namespace deployment
} // namespace openvx
#endif
