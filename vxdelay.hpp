/*
 * Copyright 2018 Stephen V C Ramm
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef _INCLUDE_VXDELAY_
#define _INCLUDE_VXDELAY_
#include "taggedtype.hpp"

namespace openvx
{
    namespace deployment
    {
        class VxDelay : public VxReference
        {
            private:
            VxDelay( vx_delay ref )
                : VxReference( reinterpret_cast<vx_reference>( ref ), VX_TYPE_DELAY )
            {
            }
            friend class VxImport;
            friend class VxContext;
            friend class VxRefArray;

            public:
            operator const vx_delay() const { return reinterpret_cast<vx_delay>( pimpl ); }
            ~VxDelay() {}
            template <vx_enum tag>
            typename  tagmap<tag>::vx_type queryDelay(typename  tagmap<tag>::vx_type init={0}) const
            {
                auto data(init);
                if ( nullptr != pimpl )
                {
                    vxQueryDelay( *this, tag, &data, sizeof( data ) );
                }
                return data;                
            }
            template <class TYPE>
            TYPE getReferenceFromDelay( vx_uint32 index )
            {
                vx_reference ref = vxGetReferenceFromDelay( *this, index );
                if ( nullptr != ref )
                {
                    vxRetainReference( ref );
                }
                return TYPE( ref );
            }

            vx_status age() { return vxAgeDelay( *this ); }
        };
    } // namespace deployment
} // namespace openvx
#endif