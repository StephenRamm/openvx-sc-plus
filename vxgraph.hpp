/*
 * Copyright 2018 Stephen V C Ramm
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef _INCLUDE_VXGRAPH_
#define _INCLUDE_VXGRAPH_
#include "taggedtype.hpp"

namespace openvx
{
    namespace deployment
    {
        class VxGraph : public VxReference
        {
            private:

            VxGraph( vx_reference ref )
                : VxReference( ref, VX_TYPE_GRAPH )
            {
            }
            friend class VxImport;
            // Arrays of reference need access to the pimpl:
            friend class VxRefArray;

            public:
            operator const vx_graph() const { return reinterpret_cast<const vx_graph>( pimpl ); }
            ~VxGraph() {}
            template <vx_enum tag>
            typename  tagmap<tag>::vx_type queryGraph(typename  tagmap<tag>::vx_type init={0}) const
            {
                auto data(init);
                if ( nullptr != pimpl )
                {
                    vxQueryGraph( *this, tag, &data, sizeof( data ) );
                }
                return data;                
            }

            vx_status setGraphParameterByIndex( vx_uint32 index, VxReference& value )
            {
                return vxSetGraphParameterByIndex( *this, index, value );
            }
            vx_status processGraph( void ) const { return vxProcessGraph( *this ); }
            vx_status scheduleGraph( void ) const { return vxScheduleGraph( *this ); }
            vx_status waitGraph( void ) const { return vxWaitGraph( *this ); }
        };

    } // namespace deployment
} // namespace openvx
#endif
