/*
 * Copyright 2018 Stephen V C Ramm
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef _INCLUDE_VXIMAGE_
#define _INCLUDE_VXIMAGE_
#include "taggedtype.hpp"

namespace openvx
{
    namespace deployment
    {
        class VxImageHandle
        {
            private:
            std::vector<vx_imagepatch_addressing_t> my_addrs;
            std::vector<void*> my_ptrs;
            vx_df_image my_color;
            vx_enum my_mem_type;

            public:
            operator const vx_imagepatch_addressing_t*() const { return my_addrs.data(); }

            operator vx_imagepatch_addressing_t*() { return my_addrs.data(); }

            operator void* const*() const { return my_ptrs.data(); }

            operator void**() { return my_ptrs.data(); }

            VxImageHandle( vx_size size )
            {
                my_addrs.resize( size );
                my_ptrs.resize( size );
            }

            vx_size size() const { return my_addrs.size(); }

            vx_df_image color() const { return my_color; }

            vx_enum mem_type() const { return my_mem_type; }
        };

        template <vx_enum usage> class VxImageMap;
        class VxImage : public VxReference
        {
            protected:
            template <vx_enum> friend class VxImageMap;
            public:
            operator const vx_image() const { return reinterpret_cast<vx_image>( pimpl ); }
            VxImage( vx_image ref )
                : VxReference( reinterpret_cast<vx_reference>( ref ), VX_TYPE_IMAGE )
            {
            }
            VxImage( vx_reference ref )
                : VxReference( ref, VX_TYPE_IMAGE )
            {
            }
            ~VxImage() {}

            template <vx_enum A>
            typename tagmap<A>::vx_type queryImage(typename tagmap<A>::vx_type init={0}) const
            {
                static_assert( (A & VX_TYPE_MASK) == (VX_TYPE_IMAGE << 8), "You must use an Image type attribute!");
                auto data(init);
                if ( nullptr != pimpl )
                {
                    vxQueryImage( *this, A, &data, sizeof( data ) );
                }
                return data;                
            }

            template <vx_enum A>
            vx_status setImageAttribute(typename tagmap<A>::vx_type data)
            {
                static_assert( (A & VX_TYPE_MASK) == (VX_TYPE_IMAGE << 8), "You must use an Image type attribute!");
                return vxSetImageAttribute(*this, A, &data, sizeof(data));
            }

            // Copying and mapping
            template<vx_enum usage>
            vx_status copyImagePatch( const vx_rectangle_t &rect,
                                        vx_uint32 plane_index,
                                        const vx_imagepatch_addressing_t &addr,
                                        void * ptr,
                                        vx_enum mem_type=VX_MEMORY_TYPE_HOST)
            {
                static_assert( (VX_READ_ONLY == usage) || (VX_WRITE_ONLY == usage), "usage must be VX_READ_ONLY or VX_WRITE_ONLY");
                return vxCopyImagePatch(*this, &rect, plane_index, &addr, ptr, usage, mem_type);
            }

            template <vx_enum usage>
            auto mapImagePatch( const vx_rectangle_t& rect,
                        vx_uint32 plane_index,
                        vx_enum mem_type=VX_MEMORY_TYPE_HOST,
                        vx_uint32 flags=0 )
            {
                return VxImageMap<usage>(*this, rect, plane_index, mem_type, flags);
            }
            
            // Child factories
            VxImage createImageFromROI( const vx_rectangle_t* rect ) { return VxImage( vxCreateImageFromROI( *this, rect ) ); }

            VxImage createImageFromChannel( enum vx_channel_e channel )
            {
                return VxImage( vxCreateImageFromChannel( *this, channel ) );
            }

            vx_status swapImageHandle( const VxImageHandle& new_ptrs, VxImageHandle& prev_ptrs )
            {
                return ( new_ptrs.size() == prev_ptrs.size() && new_ptrs.color() == prev_ptrs.color()
                         && new_ptrs.mem_type() == prev_ptrs.mem_type() )
                           ? vxSwapImageHandle( *this, new_ptrs, prev_ptrs, new_ptrs.mem_type() )
                           : VX_ERROR_INVALID_PARAMETERS;
            }

            vx_status setImageValidRectangle( const vx_rectangle_t& rect ) { return vxSetImageValidRectangle( *this, &rect ); }

            vx_status getImageValidRectangle( vx_rectangle_t& rect ) { return vxGetValidRegionImage( *this, &rect ); }
        };

        template <vx_enum usage>
        class VxImageMap
        {
            friend class VxImage;
            protected:
            // base class for image access
            // destructor is protected, so we can't instantiate
            vx_map_id map_id;
            vx_image image;
            vx_imagepatch_addressing_t addr;
            void* ptr;
            vx_enum status;
            vx_enum mem_type;
            vx_rectangle_t rect;

            VxImageMap( VxImage image_in,
                        const vx_rectangle_t& rect_in,
                        vx_uint32 plane_index,
                        vx_enum mem_type_in=VX_MEMORY_TYPE_HOST,
                        vx_uint32 flags=0 ):
                    image(image_in),
                    rect(rect_in),
                    mem_type(mem_type_in),
                    status(vxMapImagePatch( image, &rect, plane_index, &map_id, &addr, &ptr, usage, mem_type, flags ))
            {}

            public:
            ~VxImageMap()
            {
                if ( VX_SUCCESS == status && image != nullptr )
                {
                    status = vxUnmapImagePatch( image, map_id );
                }
            }

            typename const_if_RO<vx_pixel_value_t, usage>::vx_ptr formatImagePatchAddress1d( vx_uint32 index )
            {
                return ( VX_SUCCESS == status ) ? vxFormatImagePatchAddress1d( ptr, index, &addr ) : nullptr;
            }

            typename const_if_RO<vx_pixel_value_t, usage>::vx_ptr formatImagePatchAddress2d( vx_uint32 x, vx_uint32 y )
            {
                return ( VX_SUCCESS == status ) ? vxFormatImagePatchAddress2d( ptr, x, y, &addr ) : nullptr;
            }

            typename const_if_RO<void, usage>::vx_ptr getVoidPtr()
            {
                return ptr;
            }

            vx_status copyPatch( VxImage image_in, const vx_rectangle_t& rect_in, vx_uint32 image_plane_index, vx_enum usage_in )
            {
                auto my_size_x   = rect.end_x - rect.start_y;
                auto my_size_y   = rect.end_y - rect.start_y;
                auto copy_size_x = rect_in.end_x - rect_in.start_x;
                auto copy_size_y = rect_in.end_y - rect_in.start_y;
                return ( ( my_size_x < copy_size_x ) || ( my_size_y < copy_size_y ) )
                           ? VX_ERROR_INVALID_PARAMETERS
                           : vxCopyImagePatch( image_in, &rect, image_plane_index, &addr, ptr, usage_in, mem_type );
            }

            const vx_imagepatch_addressing_t* getAddressing() const { return &addr; }
        };

    } // namespace deployment
} // namespace openvx
#endif
