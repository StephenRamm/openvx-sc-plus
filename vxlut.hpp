/*
 * Copyright 2018 Stephen V C Ramm
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef _INCLUDE_VXLUT_
#define _INCLUDE_VXLUT_
#include "taggedtype.hpp"

namespace openvx
{
    namespace deployment
    {
        template <vx_enum tag, vx_enum usage> class VxLUTMap;
        template <vx_enum tag>      // A tag of zero means dynamic type
        class VxLUT : public VxReference
        {
            template <vx_enum, vx_enum> friend class VxLUTMap;
            friend class VxContext;
            VxLUT( vx_lut ref )
                : VxReference( reinterpret_cast<vx_reference>( ref ), VX_TYPE_LUT )
            {
            }
            VxLUT( vx_reference ref )
                : VxReference( ref, VX_TYPE_LUT )
            {
            }
            public:
            operator const vx_lut() const { return reinterpret_cast<vx_lut>( pimpl ); }
            ~VxLUT() {}

            template <vx_enum A>
            typename tagmap<A>::vx_type queryLUT(typename tagmap<A>::vx_type init={0}) const
            {
                static_assert( (A & VX_TYPE_MASK) == (VX_TYPE_LUT << 8), "You must use an Image type attribute!");
                auto data(init);
                if ( nullptr != pimpl )
                {
                    vxQueryLUT( *this, A, &data, sizeof( data ) );
                }
                return data;                
            }

            template <vx_enum TAG=tag>
            vx_status copyLUT(std::vector<typename tagmap<TAG>::vx_type> &data, vx_enum dir, vx_enum mem_type=VX_MEMORY_TYPE_HOST)
            {
                static_assert(TAG == tag || 0 == tag && 0 != TAG, "Invalid type for LUT copy");
                return  (queryLUT<VX_LUT_COUNT>() > data.size()) ?
                            VX_ERROR_INVALID_DIMENSION :
                            ((0==tag) && (queryLUT<VX_LUT_TYPE>() != TAG )) ?
                            VX_ERROR_INVALID_TYPE :
                            vxCopyLUT(*this, data.data(), dir, mem_type);
            }

            template <vx_enum usage=VX_READ_AND_WRITE>
            VxLUTMap<tag, usage> mapLUT(vx_enum mem_type_in=VX_MEMORY_TYPE_HOST)
            {
                return VxLUTMap<tag, usage>(*this, mem_type_in);
            }    

        };

        template <vx_enum tag, vx_enum usage>      // Can be VX_READ_ONLY, VX_READ_AND_WRITE, VX_WRITE_ONLY
        class VxLUTMap
        {
            friend class VxLUT<tag>;
            protected:
            vx_map_id map_id;
            VxLUT<tag> lut;
            void *ptr=nullptr;
            vx_size count = 0;
            vx_enum item_type = VX_TYPE_INVALID;
            vx_enum status;
            VxLUTMap( VxLUT<tag> lut_in,
                        vx_enum mem_type_in=VX_MEMORY_TYPE_HOST):
                        lut(lut_in),
                        status(vxMapLUT(lut_in, &map_id, &ptr, usage, mem_type_in, 0))
            {}
            public:
            ~VxLUTMap()
            {
                if ( VX_SUCCESS == status && lut.pimpl != nullptr )
                {
                    status = vxUnmapLUT( lut, map_id );
                }
            }
            auto getLUT() { return lut; }
            auto getStatus() { return status; }
            auto getCount()
            {
                if (0 == count && nullptr != ptr)
                    vxQueryLUT(lut, VX_LUT_COUNT, &count, sizeof(count));
                return count;
            }
            auto getType()
            {
                if (0 == tag)
                {
                    if (VX_TYPE_INVALID == item_type)
                        vxQueryLUT(lut, VX_LUT_TYPE, &item_type, sizeof(item_type));
                    return item_type;
                }
                else
                    return tag;
            }
            template <vx_enum TAG=tag>
            auto getPtr() 
            { 
                static_assert( 0 == tag || tag == TAG && 0 != TAG, "VxLUTMap.getPtr() with wrong or invalid type");
                if ( getType() == TAG)
                    return (typename const_if_RO<typename tagmap<TAG>::vx_type, usage>::vx_ptr)ptr;
                else
                    throw std::invalid_argument("Attempt to get incorrect type from LUT");
            }
            
            template <vx_enum T=tag>
            auto & operator[](vx_size index)
            {
                if (index < getCount() && nullptr != ptr)
                    return getPtr<T>()[index];
                else
                    throw std::invalid_argument("Index out of range for LUTMap.ix(index)");
            }
        };
    } // namespace deployment
} // namespace openvx
#endif
