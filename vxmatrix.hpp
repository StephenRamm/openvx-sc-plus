/*
 * Copyright 2018 Stephen V C Ramm
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef _INCLUDE_VXMATRIX_
#define _INCLUDE_VXMATRIX_
#include "taggedtype.hpp"

namespace openvx
{
    namespace deployment
    {
        template <vx_enum data_type>
        class VxMatrix : public VxReference
        {
            VxMatrix( vx_matrix ref )
                : VxReference( reinterpret_cast<vx_reference>( ref ), VX_TYPE_MATRIX )
            {
            }
            VxMatrix( vx_reference ref )
                : VxReference( ref, VX_TYPE_MATRIX )
            {
            }
            friend class VxContext;
            public:
            operator const vx_matrix() const { return reinterpret_cast<vx_matrix>( pimpl ); }
            ~VxMatrix() {}

            template <vx_enum A>
            typename tagmap<A>::vx_type queryMatrix(typename tagmap<A>::vx_type init={0}) const
            {
                auto data(init);
                if ( nullptr != pimpl )
                {
                    vxQueryMatrix( *this, A, &data, sizeof( data ) );
                }
                return data;                
            }

            vx_status copyMatrix(std::vector<typename tagmap<data_type>::vx_type> data, vx_enum usage, vx_enum memory_type)
            {
                if ( queryMatrix<VX_MATRIX_SIZE>() <= data.size()*sizeof(typename tagmap<data_type>::vx_type))
                    return vxCopyMatrix(*this, data.data(), usage, memory_type);
                else
                    return VX_ERROR_INVALID_PARAMETERS;
            }

        };

    } // namespace deployment
} // namespace openvx
#endif
