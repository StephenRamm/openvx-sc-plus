/*
 * Copyright 2018 Stephen V C Ramm
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef _INCLUDE_VXOBJECTARRAY_
#define _INCLUDE_VXOBJECTARRAY_
#include "taggedtype.hpp"

namespace openvx
{
    namespace deployment
    {
        class VxObjectArray : public VxReference
        {
            private:
            VxObjectArray( vx_object_array ref )
                : VxReference( reinterpret_cast<vx_reference>( ref ), VX_TYPE_OBJECT_ARRAY )
            {
            }
            VxObjectArray( vx_reference ref )
                : VxReference( ref, VX_TYPE_OBJECT_ARRAY )
            {
            }

            friend class VxImport;
            friend class VxContext;
            friend class VxRefArray;
            template <vx_enum> friend class VxTensor;

            public:
            operator const vx_object_array() const { return reinterpret_cast<vx_object_array>( pimpl ); }
            ~VxObjectArray() {}
            // general-purpose query function.
            // (Could be made to raise an exception if vxQueryObjectArray
            // returns an error)
            template <vx_enum tag>
            typename  tagmap<tag>::vx_type queryObjectArray(typename  tagmap<tag>::vx_type init={0}) const
            {
                auto data(init);
                if ( nullptr != pimpl )
                {
                    vxQueryObjectArray( *this, tag, &data, sizeof( data ) );
                }
                return data;                
            }

            template <class TYPE>
            TYPE getObjectArrayItem( vx_uint32 index )
            {
                return TYPE( vxGetObjectArrayItem( *this, index ) );
            }

            template <class T>
            T operator [](vx_size index) { return getObjectArrayItem<T>(index); }
        };
    } // namespace deployment
} // namespace openvx
#endif
