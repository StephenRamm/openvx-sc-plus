/*
 * Copyright 2018 Stephen V C Ramm
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef _INCLUDE_VXPYRAMID_
#define _INCLUDE_VXPYRAMID_
#include "taggedtype.hpp"

namespace openvx
{
    namespace deployment
    {
        class VxPyramid : public VxReference
        {
            friend class VxContext;
            VxPyramid( vx_pyramid ref )
                : VxReference( reinterpret_cast<vx_reference>( ref ), VX_TYPE_PYRAMID )
            {
            }
            VxPyramid( vx_reference ref )
                : VxReference( ref, VX_TYPE_PYRAMID )
            {
            }
            public:
            operator const vx_pyramid() const { return reinterpret_cast<vx_pyramid>( pimpl ); }
            ~VxPyramid() {}

            template <vx_enum A>  // use a tagged type
            typename tagmap<A>::vx_type queryPyramid(typename tagmap<A>::vx_type init={0}) const
            {
                auto data(init);
                if ( nullptr != pimpl )
                {
                    vxQueryPyramid( *this, A, &data, sizeof( data ) );
                }
                return data;                
            }

            VxImage getPyramidLevel(vx_uint32 index)
            {
                return vxGetPyramidLevel(*this, index);
            }
        };
    } // namespace deployment
} // namespace openvx
#endif
