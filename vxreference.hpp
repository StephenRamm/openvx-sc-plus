/*
 * Copyright 2018 Stephen V C Ramm
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef _INCLUDE_VXREFERENCE_
#define _INCLUDE_VXREFERENCE_
#include "taggedtype.hpp"

namespace openvx
{
    namespace deployment
    {
        class VxContext;
        class VxGraph;
        class VxReference
        {
            public:
            // Get the context from this reference
            VxContext getContext();

            // There are no virtual functions in this class.
            // Objects of type VxReference may not be instantiated.
            protected:
            // remember that the OpenVX C definition of vx_reference is a
            // pointer to an opaque type Derived classes will need to be able to
            // access the implementation pointer, but the application using this
            // shim should not.
            vx_reference pimpl;

            // Any of the derived classes and friends may cast VxReference or
            // derived class to vx_reference.
            operator const vx_reference() const { return reinterpret_cast<const vx_reference>( pimpl ); }

            public:
            template <vx_enum A>       // use the attribute name
            typename tagmap<A>::vx_type queryReference(typename tagmap<A>::vx_type init={0}) const
            {
                static_assert( (A & VX_TYPE_MASK) == (VX_TYPE_REFERENCE << 8), "You must use a Reference type attribute!");
                auto data(init);
                if ( nullptr != pimpl )
                {
                    vxQueryReference( pimpl, A, &data, sizeof( data ) );
                }
                return data;                
            }

            protected:
            // Protected constructor. VxReference can't be made without a pimpl.
            // pimpl must match given type enum or it's set to nullptr
            VxReference( vx_reference ref, vx_enum vxtype )
                : pimpl( ref )
            {
                // In this constructor we make sure that the reference count of
                // the OpenVX object is decremented if the pointer is the wrong
                // type
                if ( nullptr != pimpl )
                {
                    vx_enum the_type = queryReference<VX_REFERENCE_TYPE>();
                    if ( the_type != vxtype )
                    {
                        vxReleaseReference( &ref );
                        pimpl = nullptr;
                    }
                }
                
            }

            ~VxReference( void )
            {
                // Protected destructor. VxReference objects are not allowed!
                // In this destructor we make sure that the underlying OpenVX
                // object has its reference count decremented, so that the
                // implementation will destroy it when we've removed all our
                // references to it. Notice we take care never to pass a nullptr
                // pointer to any OpenVx APIs We rely upon the OpenVX API to
                // call the correct ReleaseXXX function when VxReleaseReference
                // is called.
                if ( nullptr != pimpl )
                {
                    vxReleaseReference( &pimpl );
                }
            }

            VxReference( const VxReference& obj )
            {
                // In the copy constructor we make sure that the reference count
                // of the OpenVX object is incremented when we take a copy of
                // the pointer
                pimpl = obj.pimpl;
                if ( nullptr != pimpl )
                {
                    vxRetainReference( pimpl );
                }
            }

            friend class VxGraph;
            friend class VxRefArray;
            friend class VxContext;

            public:
            void operator=( const VxReference& obj )
            {
                // We define the assignment operator so as to be able to keep
                // track of the reference counts. An alternative would be to
                // raise an error if assigning to a reference that already has
                // a value in pimpl since this is most likely an error.
                // Notice that the operator does not return a value and hence
                // constructs of the type a = b = c cannot be made.
                if ( nullptr != pimpl )
                {
                    vxReleaseReference( &pimpl );
                }
                pimpl = obj.pimpl;
                if ( nullptr != pimpl )
                {
                    vxRetainReference( pimpl );
                }
            }

            // getName() is included for utility
            const std::string getName() const
            {
                return queryReference<VX_REFERENCE_NAME>("");
            }

            vx_status setName( const std::string& name ) { return vxSetReferenceName( pimpl, name.c_str() ); }

            // The vxHint function allows implementations to define various
            // hints taking different data types; However, they must first 
            // define the corresponding tagmap struct to map the hint to
            // the data type.
            template <enum vx_hint_e attribute>
            vx_status hint(typename tagmap<attribute>::vx_type & data )
            {
                return vxHint( pimpl, attribute, static_cast<void*>( data ), sizeof( data ) );
            }

            // Other VxReference methods simply match directly to the OpenVX
            // API:
            vx_status directive( enum vx_directive_e directive ) { return vxDirective( pimpl, directive ); }

            vx_status getStatus( void ) const { return vxGetStatus( pimpl ); }
        };

        class VxRefArray
        {
            // Used to handle arrays of references for VxImport factory defined
            // in VxContext
            private:
            std::vector<vx_reference> ref_array;
            std::vector<vx_enum> use_array;
            operator vx_reference*() { return ref_array.data(); }
            operator const vx_enum*() { return use_array.data(); }

            // We dont' allow copies of this
            // If for some reason an assignment operator is required,
            // then please remember to call vxRetainReference for
            // each non-nullptr entry.
            VxRefArray& operator=( VxRefArray& ) {}
            friend class VxContext;

            public:
            VxRefArray( vx_size numrefs )
            {
                ref_array.resize( numrefs );
                use_array.resize( numrefs );
                for (auto i = 0; i < numrefs; ++i )
                {
                    ref_array[i] = nullptr;
                    use_array[i] = VX_IX_USE_EXPORT_VALUES;
                }
            }

            ~VxRefArray()
            {
                for ( vx_reference ref : ref_array )
                {
                    if ( nullptr != ref )
                    {
                        vxReleaseReference( &ref );
                    }
                }
            }

            vx_status put( vx_size index, const VxReference& obj, vx_enum use = VX_IX_USE_EXPORT_VALUES )
            {
                vx_status status = VX_SUCCESS;
                if ( index < ref_array.size() )
                {
                    if ( nullptr != ref_array[index] )
                    {
                        vxReleaseReference( &ref_array[index] );
                    }
                    ref_array[index] = obj;
                    use_array[index] = use;
                    if ( nullptr != ref_array[index] )
                    {
                        vxRetainReference( ref_array[index] );
                    }
                }
                else
                {
                    status = VX_FAILURE;
                }
                return status;
            }

            template <class TYPE>
            TYPE getRef( vx_size index )
            {
                // Only allowed to get VxReference & derived types from a
                // VxRefArray, or vx_image or vx_reference
                static_assert( std::is_base_of<VxReference, TYPE>::value || 
                               std::is_same<vx_reference, TYPE>::value || 
                               std::is_same<vx_image, TYPE>::value, 
                               "VxRefArray::get<TYPE> : TYPE must be derived from VxReference" );
                if ( index < ref_array.size() && ( nullptr != ref_array[index] ) )
                {
                    vxRetainReference( ref_array[index] );
                    return TYPE( ref_array[index] );
                }
                else
                {
                    return TYPE( ( vx_reference ) nullptr );
                }
            }

            vx_enum getUse( vx_size index ) const
            {
                return ( index < use_array.size() ) ? use_array[index] : VX_IX_USE_APPLICATION_CREATE;
            }

            vx_size getSize() const { return ref_array.size(); }
        };
    } // namespace deployment
} // namespace openvx
#endif
