/*
 * Copyright 2018 Stephen V C Ramm
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef _INCLUDE_VXREMAP_
#define _INCLUDE_VXREMAP_
#include "taggedtype.hpp"

namespace openvx
{
    namespace deployment
    {
        template <vx_enum usage>
        class VxRemapMap;
        class VxRemap : public VxReference
        {
            template <vx_enum> friend class VxRemapMap;
            friend class VxContext;
            VxRemap( vx_remap ref )
                : VxReference( reinterpret_cast<vx_reference>( ref ), VX_TYPE_REMAP )
            {
            }
            VxRemap( vx_reference ref )
                : VxReference( ref, VX_TYPE_REMAP )
            {
            }
            public:
            operator const vx_remap() const { return reinterpret_cast<vx_remap>( pimpl ); }
            ~VxRemap() {}

            template <vx_enum A>
            typename tagmap<A>::vx_type queryRemap(typename tagmap<A>::vx_type init={0}) const
            {
                auto data(init);
                if ( nullptr != pimpl )
                {
                    vxQueryRemap( *this, A, &data, sizeof( data ) );
                }
                return data;                
            }

            vx_status copyRemapPatch(const vx_rectangle_t &rect, vx_size user_stride_y, vx_coordinates2df_t *ptr, vx_enum usage, vx_enum mem_type=VX_MEMORY_TYPE_NONE)
            {
                return vxCopyRemapPatch(*this, &rect, user_stride_y, ptr, VX_TYPE_COORDINATES2DF, usage, mem_type);
            }

            template <vx_enum usage=VX_READ_AND_WRITE>
            auto mapRemapPatch(const vx_rectangle_t &rect, vx_enum mem_type=VX_MEMORY_TYPE_NONE)
            {
                return VxRemapMap<usage>( *this, rect, mem_type);
            } 
            // vx_status setRemapPoint(vx_uint32 dst_x, vx_uint32 dst_y, vx_float32 src_x, vx_float32 src_y)
            // {
            //     return vxSetRemapPoint(*this, dst_x, dst_y, src_x, src_y);
            // }

            // vx_status getRemapPoint(vx_uint32 dst_x, vx_uint32 dst_y, vx_float32 &src_x, vx_float32 &src_y)
            // {
            //     return vxGetRemapPoint(*this, dst_x, dst_y, &src_x, &src_y);
            // }
        };

        template <vx_enum usage=VX_READ_AND_WRITE>
        class VxRemapMap
        {
            friend class VxRemap;
            protected:
            vx_map_id map_id;
            VxRemap remap;
            vx_status status;
            vx_size stride_y;
            vx_coordinates2df_t *ptr;
            vx_rectangle_t rect;
            VxRemapMap(VxRemap remap_in, const vx_rectangle_t &rect_in, vx_enum mem_type=VX_MEMORY_TYPE_NONE):
                remap(remap_in),
                rect(rect_in),
                status(vxMapRemapPatch(remap_in, &rect_in, &map_id, &stride_y, &ptr, VX_TYPE_COORDINATES2DF, usage, mem_type))
            {}
            public:
            ~VxRemapMap()
            {
                if ( VX_SUCCESS == status && remap.pimpl != nullptr )
                {
                    status = vxUnmapRemapPatch( remap, map_id );
                }
            }
            auto getRemap() { return remap; }
            auto getStatus() { return status; }
            auto getStrideY() { return stride_y; }
            auto getPtr() 
            { 
                return (typename const_if_RO<vx_coordinates2df_t, usage>::vx_ptr)ptr;
            }
            
            auto getLinePtr(vx_uint32 y)
            {
                if ( y < rect.end_y - rect.start_y)
                    return (const_if_RO<vx_coordinates2df_t, usage>::vx_ptr)(((vx_uint8 *)ptr)[stride_y]);
                else
                    throw std::invalid_argument("Y coordinate out of range for VxRemapMap index");
            }

            auto getPointPtr(vx_uint32 x, vx_uint32 y)
            {
                if ( x < rect.end_x - rect.start_x)
                    return getLinePtr(y)[x];
                else
                    throw std::invalid_argument("Y coordinate out of range for VxRemapMap index");
            }
        };
    } // namespace deployment
} // namespace openvx
#endif
