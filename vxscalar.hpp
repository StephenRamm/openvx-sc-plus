/*
 * Copyright 2018 Stephen V C Ramm
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef _INCULDE_VXSCALAR_
#define _INCULDE_VXSCALAR_
#include "taggedtype.hpp"

namespace openvx
{
    namespace deployment
    {
        template<vx_enum tag>       // Note, if tag == 0, then type is dynamic
        class VxScalar : public VxReference
        {
            private:
            VxScalar( vx_scalar ref )
                : VxReference( reinterpret_cast<vx_reference>( ref ), VX_TYPE_SCALAR )
            {
            }
            friend class VxContext;
            public:
            operator const vx_scalar() const { return reinterpret_cast<vx_scalar>( pimpl ); }
            ~VxScalar() {}
            template <vx_enum A>
            typename tagmap<A>::vx_type queryScalar(typename tagmap<A>::vx_type init={0}) const
            {
                auto data(init);
                if ( nullptr != pimpl )
                {
                    vxQueryScalar( *this, A, &data, sizeof( data ) );
                }
                return data;                
            }

            template <vx_enum TAG=tag>
            vx_status copyScalar(typename tagmap<TAG>::vx_type &data, vx_enum dir, vx_enum mem_type=VX_MEMORY_TYPE_HOST)
            {
                static_assert( 0 == tag || tag == TAG && 0 != TAG, "copyScalar with wrong or invalid type");
                if ((0 == tag) && (queryScalar<VX_SCALAR_TYPE>() == TAG) || tag == TAG)
                {
                    return vxCopyScalar(*this, &data, dir, mem_type);
                }
                else
                    // This could be made more informative by saying what types are involved
                    throw std::invalid_argument("Attempt to copy incorrect type to or from scalar");
            }

            // set and get for the value included for ease of use
            template <vx_enum TAG=tag>
            typename tagmap<TAG>::vx_type getScalarValue(typename tagmap<TAG>::vx_type init={0}) // T - use a tagged type. must match exactly the type of the scalar
            {
                auto data(init);
                copyScalar<TAG>(data, VX_READ_ONLY);
                return data;
            }

            template <vx_enum TAG=tag>
            vx_status setScalarValue(typename tagmap<TAG>::vx_type data)
            {
                return copyScalar<TAG>(data, VX_WRITE_ONLY);
            }
        };

    } // namespace deployment
} // namespace openvx
#endif
