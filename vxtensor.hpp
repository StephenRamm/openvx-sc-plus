/*
 * Copyright 2018 Stephen V C Ramm
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef _INCLUDE_VXTENSOR_
#define _INCLUDE_VXTENSOR_
#include "taggedtype.hpp"

namespace openvx
{
    namespace deployment
    {
        template<vx_enum data_type>
        class VxTensor : public VxReference
        {
            private:
            VxTensor( vx_tensor ref )
                : VxReference( reinterpret_cast<vx_reference>( ref ), VX_TYPE_TENSOR )
            {
            }
            friend class VxContext;
            public:
            operator const vx_tensor() const { return reinterpret_cast<vx_tensor>( pimpl ); }
            ~VxTensor() {}
            template <vx_enum A>
            typename tagmap<A>::vx_type queryTensor(typename tagmap<A>::vx_type init={0}) const
            {
                auto data(init);
                if ( nullptr != pimpl )
                {
                    vxQueryTensor( *this, A, &data, sizeof( data ) );
                }
                return data;                
            }
            
            // The data for the tensor patch is defined in the standard as a pointer to void. Here we specify the
            // type according to the data_type that the tensor was created with, so get some extra compile-time
            // checking.
            template<vx_size n_dims>
            vx_status copyTensorPatch(std::array<vx_size, n_dims> view_start,
                                      std::array<vx_size, n_dims> view_end,
                                      std::array<vx_size, n_dims> user_stride,
                                      typename tagmap<data_type>::vx_type * user_ptr,
                                      vx_enum usage, vx_enum user_memory_type)
            {
                return vxCopyTensorPatch(*this, n_dims, view_start.data(), view_end.data(),
                                         user_stride.data(), (void *)user_ptr, usage, user_memory_type);
            }

            // We could have the image_format as a template parameter and check that it is compatible
            // with the tensor data_type...
            VxObjectArray createImageObjectArrayFromTensor(const vx_rectangle_t & rect,
                                                           vx_size array_size, vx_size jump,
                                                           vx_df_image image_format)
            {
                return vxCreateImageObjectArrayFromTensor(*this, &rect, array_size, jump, image_format);
            }

            template<vx_size n_dims>
            VxTensor<data_type> createTensorFromView(std::array<vx_size, n_dims> view_start,
                                          std::array<vx_size, n_dims> view_end)
            {
                return vxCreateTensorFromView(*this, n_dims, view_start.data(), view_end.data());
            }
        };
       
    } // namespace deployment
} // namespace openvx
#endif
