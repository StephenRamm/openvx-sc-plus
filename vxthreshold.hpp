/*
 * Copyright 2018 Stephen V C Ramm
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef _INCLUDE_VXTHRESHOLD_
#define _INCLUDE_VXTHRESHOLD_
#include "taggedtype.hpp"

namespace openvx
{
    namespace deployment
    {
        class VxThreshold : public VxReference
        {
            private:
            VxThreshold( vx_threshold ref )
                : VxReference( reinterpret_cast<vx_reference>( ref ), VX_TYPE_THRESHOLD )
            {
            }
            friend class VxContext;
            public:
            operator const vx_threshold() const { return reinterpret_cast<vx_threshold>( pimpl ); }
            ~VxThreshold() {}
            template <vx_enum A>
            typename tagmap<A>::vx_type queryThreshold(typename tagmap<A>::vx_type init={0}) const
            {
                auto data(init);
                if ( nullptr != pimpl )
                {
                    vxQueryThreshold( *this, A, &data, sizeof( data ) );
                }
                return data;                
            }
            vx_status copyThresholdOutput(vx_pixel_value_t* true_value_ptr,
                                          vx_pixel_value_t* false_value_ptr,
                                          vx_enum usage, vx_enum user_mem_type)
            {
                return vxCopyThresholdOutput(*this, true_value_ptr, false_value_ptr,
                                              usage, user_mem_type);
            }
            vx_status copyThresholdRange(vx_pixel_value_t* lower_value_ptr,
                                         vx_pixel_value_t* upper_value_ptr,
                                         vx_enum usage, vx_enum user_mem_type)
            {
                return vxCopyThresholdRange(*this, lower_value_ptr, upper_value_ptr,
                                              usage, user_mem_type);
            }
            vx_status copyThresholdValue(vx_pixel_value_t* value_ptr,
                                          vx_enum usage, vx_enum user_mem_type)
            {
                return vxCopyThresholdValue(*this, value_ptr,
                                              usage, user_mem_type);
            }
        };

    } // namespace deployment
} // namespace openvx
#endif